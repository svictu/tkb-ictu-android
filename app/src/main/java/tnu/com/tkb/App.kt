package tnu.com.tkb

import android.app.Application
import com.splunk.mint.Mint

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Mint.initAndStartSession(this, "cb8ee20f")
    }
}