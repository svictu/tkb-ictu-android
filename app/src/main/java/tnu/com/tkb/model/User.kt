package tnu.com.tkb.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class User(
//        /*@SerializedName("sv")*/ @Json(name = "name") val name: String,
//        /*@SerializedName("lop")*/ @Json(name = "lop") val className: String,
//        /*@SerializedName("msv") */@Json(name = "msv") val id: String,
//        /* @SerializedName("tkb") */@Json(name = "tkb") val tkb: List<Tkb>
        @SerializedName("sv") val name: String,
        @SerializedName("lop") val className: String,
        @SerializedName("msv") val id: String,
        @SerializedName("tkb") val tkb: List<Tkb>
)