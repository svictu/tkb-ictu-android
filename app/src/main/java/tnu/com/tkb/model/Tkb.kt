package tnu.com.tkb.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Tkb(
        @SerializedName("tiet") val time: String,
        @SerializedName("mon") val name: String,
        @SerializedName("kieu") val type: String,
        @SerializedName("diadiem") val place: String,
        @SerializedName("giangvien") val teacher: String,
        @SerializedName("ngay") var date: String
) {
    fun toInfo(): String {
        return "Môn: $name\nTiết: $time\nĐịa điểm: $place\nGiảng viên: $teacher\nKiểu: $type"
    }
}