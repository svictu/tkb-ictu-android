package tnu.com.tkb.calendarevent;

public class TKBEvent {
    private int color; // color get by ContextCompat
    private int lineDraw = 0; // line draw in day of month view

    public int getColor() {
        return color;
    }

    public TKBEvent setColor(int color) {
        this.color = color;
        return this;
    }

    public int getLineDraw() {
        return lineDraw;
    }

    public TKBEvent setLineDraw(int lineDraw) {
        this.lineDraw = lineDraw;
        return this;
    }
}
