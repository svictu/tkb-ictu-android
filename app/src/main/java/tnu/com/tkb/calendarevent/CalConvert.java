package tnu.com.tkb.calendarevent;

import android.content.Context;

import java.util.Calendar;
import java.util.Date;

import duc.cal.CalConverterVN;
import duc.cal.LunarDate;
import tnu.com.tkb.R;

public class CalConvert {
    public static final int SHORT = 0;
    public static final int LONG = 1;
    public static final int NUMBER = 2;

    public static final int DAY = 0;
    public static final int MONTH = 1;

    public static String[] holidaySolar = {"1/1", "30/4", "1/5", "2/9"};
    public static String[] holidayLunar = {"1/1", "2/1", "3/1", "10/3"};

    public static LunarDate[] getMonthLunar(int mm, int yy) {
        CalConverterVN convert = new CalConverterVN();
        return convert.computeLunarDates(mm, yy);
    }

    public static LunarDate getLunarDate(int dd, int mm, int yy) {
        CalConverterVN convert = new CalConverterVN();
        return convert.computeLunarDate(dd, mm, yy);
    }

    public static String getLunarString(LunarDate lunarDate) {
        String lunarString = lunarDate.getDay() + "";
        if (lunarDate.getDay() == 1) {
            lunarString += "/" + lunarDate.getMonth();
        }
        return lunarString;
    }

    public static String[] getArrayName(Context context, int longOrShortOrNum, int monthOrDay) {
        int id = R.array.short_day;
        if (longOrShortOrNum == SHORT) {
            if (monthOrDay == DAY) {
                id = R.array.short_day;
            } else {
                id = R.array.short_month;
            }
        } else if (longOrShortOrNum == LONG) {
            if (monthOrDay == DAY) {
                id = R.array.long_day;
            } else {
                id = R.array.long_month;
            }
        } else if (longOrShortOrNum == NUMBER) {
            if (monthOrDay == DAY) {
                id = R.array.number_day;
            } else {
                id = R.array.number_month;
            }
        }
        return context.getResources().getStringArray(id);
    }

    public static int getPositionBegin(Calendar month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(month.getTime());
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int begin = calendar.get(Calendar.DAY_OF_WEEK);
        return begin > 1 ? begin - 2 : 6;
    }

    /**
     * @param month from 1 to 12
     */
    public static boolean isHoliday(int day, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month - 1, day);

        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        }

        String solarString = day + "/" + month;

        for (String holiday : holidaySolar) {
            if (solarString.equals(holiday)) {
                return true;
            }
        }

        LunarDate lunarDate = CalConvert.getLunarDate(day, month, year);
        String lunarString = lunarDate.getDay() + "/" + lunarDate.getMonth();

        for (String holiday : holidayLunar) {
            if (lunarString.equals(holiday)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isHoliday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return isHoliday(calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
    }


    /**
     * @param monthSolar from 1 to 12
     */
    public static String getEvent(Context context, int daySolar, int monthSolar, int year) {

        LunarDate lunarDate = CalConvert.getLunarDate(daySolar, monthSolar, year);

        int dayLunar = lunarDate.getDay();
        int monthLunar = lunarDate.getMonth();

        String[] arr = context.getResources().getStringArray(R.array.event);
        for (String anArr : arr) {
            String[] item = anArr.split("_");
            boolean isSolar = (item[0].equals("solar"));
            int dayEvent = Integer.parseInt(item[1]);
            int monthEvent = Integer.parseInt(item[2]);
            String content = item[3];

            if (isSolar) {
                if (daySolar == dayEvent && monthSolar == monthEvent) {
                    return content;
                }
            } else {
                if (dayLunar == dayEvent && monthLunar == monthEvent) {
                    return content;
                }
            }
        }
        return null;
    }

    public static String getEvent(Context context, Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getEvent(context, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
    }
}