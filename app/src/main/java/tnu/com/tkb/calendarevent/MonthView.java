package tnu.com.tkb.calendarevent;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.core.content.ContextCompat;
import duc.cal.LunarDate;
import tnu.com.tkb.R;

public class MonthView extends View {

    public static final String DATE_VN_FORMAT = "dd/MM/yyyy";

    private Typeface mTypeface = Typeface.DEFAULT;
    private int mTextSize = -1;
    private int mTextColor = 0xFF000000;
    private int mTextLabelColor = 0xFF767676;
    private int mTextHighlightColor = 0xFFFFFFFF;
    private int mTextDisableColor;
    private int mSelectionColor;
    private int mAnimDuration = -1;
    private int mHolidayColor;
    private int mClickColor;
    private Interpolator mInInterpolator;
    private Interpolator mOutInterpolator;

    private Paint mPaint;
    private float mDayBaseWidth;
    private float mDayBaseHeight;
    private float mDayHeight;
    private float mDayWidth;
    private int mDayPadding;
    private float mSelectionRadius;
    private String[] mLabels;

    private long mStartTime;
    private float mAnimProgress;
    private boolean mRunning;

    private int mTouchedDay = -1;

    private int mMonth;
    private int mYear;
    private int mMaxDay;
    private int mFirstDayCol;
    private int mMinAvailDay = -1;
    private int mMaxAvailDay = -1;
    private int mToday = -1;
    private int mSelectedDay = -1;
    private int mPreviousSelectedDay = -1;
    private String mMonthText;

    private Calendar mCalendar;
    private int mFirstDayOfWeek;

    private int mMonthRealWidth;
    private int mMonthRealHeight;
    private int mPaddingLeft = 0;
    private int mPaddingTop = 10;
    private int mPaddingRight = 0;
    private int mPaddingBottom = 10;

    private float strokeWidthDefault;
    private float strokeWidthCircle = 3;

    private Context context;

    private HashMap<String, List<TKBEvent>> mapEvent = new HashMap<>();

    private int clickDay = -1;

    public void addEvent(String day, TKBEvent event) {
        List<TKBEvent> eventList = new ArrayList<>();
        if (mapEvent.containsKey(day)) {
            eventList = mapEvent.get(day);
        }
        eventList.add(event);
        mapEvent.put(day, eventList);
    }

    public void clearEvent() {
        mapEvent.clear();
    }

    public void setHolidayColor(int holidayColor) {
        mHolidayColor = holidayColor;
    }

    public void setFirstDayOfWeek(int firstDayOfWeek) {
        mFirstDayOfWeek = firstDayOfWeek;
    }

    public MonthView(Context context, Calendar calendar) {
        super(context);
        this.context = context;
        this.mCalendar = calendar;
        setWillNotDraw(false);

        init();

        setMonth(mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.YEAR));

    }

    static class ThemeUtil {
        public static final long FRAME_DURATION = 1000 / 60;

        public static int colorPrimary(Context context, int defaultColor) {
            return ContextCompat.getColor(context, R.color.colorPrimary);
        }

        public static int colorAccent(Context context, int defaultColor) {
            return ContextCompat.getColor(context, R.color.colorAccent);
        }

        public static int dpToPx(Context context, int dp) {
            return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics()) + 0.5f);
        }
    }

    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setTextAlign(Paint.Align.CENTER);
        strokeWidthDefault = mPaint.getStrokeWidth();

        mDayPadding = ThemeUtil.dpToPx(context, 4);

        mSelectionColor = ThemeUtil.colorPrimary(context, 0xFF000000);
        mHolidayColor = ThemeUtil.colorPrimary(context, 0xFF000000);
        mClickColor = ThemeUtil.colorAccent(context, 0xFF000000);


        mFirstDayOfWeek = mCalendar.getFirstDayOfWeek();
        mLabels = CalConvert.getArrayName(context, CalConvert.SHORT, CalConvert.DAY);
        mInInterpolator = new DecelerateInterpolator();
        mOutInterpolator = new DecelerateInterpolator();
        mAnimDuration = context.getResources().getInteger(android.R.integer.config_mediumAnimTime);
        setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
    }

    public void setMonth(int month, int year) {

        Calendar cal = Calendar.getInstance();
        if (mCalendar.get(Calendar.MONTH) == cal.get(Calendar.MONTH) && mCalendar.get(Calendar.YEAR) == cal.get(Calendar.YEAR)) {
            mToday = cal.get(Calendar.DAY_OF_MONTH);
            mSelectedDay = cal.get(Calendar.DAY_OF_MONTH);
        } else {
            mToday = -1;
            mSelectedDay = -1;
        }

        if (mMonth != month || mYear != year) {
            mMonth = month;
            mYear = year;
            calculateMonthView();
            invalidate();
        }
    }

    public void setSelectedDay(int day, boolean animation) {
        if (mSelectedDay != day) {
            mPreviousSelectedDay = mSelectedDay;
            mSelectedDay = day;

            if (animation)
                startAnimation();
            else
                invalidate();
        }
    }

    public void setToday(int day) {
        if (mToday != day) {
            mToday = day;
            invalidate();
        }
    }

    public void setAvailableDay(int min, int max) {
        if (mMinAvailDay != min || mMaxAvailDay != max) {
            mMinAvailDay = min;
            mMaxAvailDay = max;
            invalidate();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mDayWidth = (w - mPaddingLeft - mPaddingRight) / 7f;
        mDayHeight = (h - mDayBaseHeight - mDayPadding * 2 - mPaddingTop - mPaddingBottom) / 7f;
        mSelectionRadius = Math.min(mDayWidth, mDayHeight) / 2f - 2;
    }

    private void calculateMonthView() {


        mCalendar.set(Calendar.DAY_OF_MONTH, 1);
        mCalendar.set(Calendar.MONTH, mMonth);
        mCalendar.set(Calendar.YEAR, mYear);

        mFirstDayOfWeek = mCalendar.getFirstDayOfWeek();

        mMaxDay = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int dayOfWeek = mCalendar.get(Calendar.DAY_OF_WEEK);
        mFirstDayCol = dayOfWeek < mFirstDayOfWeek ? dayOfWeek + 7 - mFirstDayOfWeek : dayOfWeek - mFirstDayOfWeek;
        mMonthText = getDisplayName(context, CalConvert.NUMBER, CalConvert.MONTH, mMonth) + " - " + mYear;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measureMonthView(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(mMonthRealWidth, mMonthRealHeight);
    }

    public String getmMonthText() {
        return mMonthText;
    }

    private boolean isMonthText = false;

    public boolean isMonthText() {
        return isMonthText;
    }

    public void setIsMonthText(boolean isMonthText) {
        this.isMonthText = isMonthText;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mTextSize = context.getResources().getDimensionPixelOffset(R.dimen.abc_text_size_menu_material);

        mPaint.setTextSize(mTextSize);
        mPaint.setTypeface(mTypeface);
        mPaint.setFakeBoldText(true);
        mPaint.setColor(mTextColor);

        float x = 3.5f * mDayWidth + getPaddingLeft();
        float y = getPaddingTop();
        if (isMonthText) {
            y += mDayPadding * 2 + mDayBaseHeight;
            canvas.drawText(mMonthText.toUpperCase(), x, y, mPaint);
        }

        //draw selection
        float paddingLeft = getPaddingLeft();
        float paddingTop = getPaddingTop();
        if (isMonthText) {
            paddingTop += mDayPadding * 2 + mDayBaseHeight;
        }

        if (mSelectedDay > 0) {
            int col = (mFirstDayCol + mSelectedDay - 1) % 7;
            int row = (mFirstDayCol + mSelectedDay - 1) / 7 + 1;

            x = (col + 0.5f) * mDayWidth + paddingLeft;
            y = (row + 0.5f) * mDayHeight + paddingTop;
            float radius = mRunning ? mInInterpolator.getInterpolation(mAnimProgress) * mSelectionRadius : mSelectionRadius;
            mPaint.setColor(mSelectionColor);
            mPaint.setStrokeWidth(strokeWidthCircle);
            canvas.drawCircle(x, y, radius, mPaint);
            mPaint.setStrokeWidth(strokeWidthDefault);
        }

        if (mRunning && mPreviousSelectedDay > 0) {
            int col = (mFirstDayCol + mPreviousSelectedDay - 1) % 7;
            int row = (mFirstDayCol + mPreviousSelectedDay - 1) / 7 + 1;

            x = (col + 0.5f) * mDayWidth + paddingLeft;
            y = (row + 0.5f) * mDayHeight + paddingTop;
            float radius = (1f - mOutInterpolator.getInterpolation(mAnimProgress)) * mSelectionRadius;
            mPaint.setColor(mSelectionColor);
            mPaint.setStrokeWidth(strokeWidthCircle);
            canvas.drawCircle(x, y, radius, mPaint);
            mPaint.setStrokeWidth(strokeWidthDefault);
        }

        if (clickDay > 0) {
            int col = (mFirstDayCol + clickDay - 1) % 7;
            int row = (mFirstDayCol + clickDay - 1) / 7 + 1;

            x = (col + 0.5f) * mDayWidth + paddingLeft;
            y = (row + 0.5f) * mDayHeight + paddingTop;
            float radius = (1f - mOutInterpolator.getInterpolation(mAnimProgress)) * mSelectionRadius;
            mPaint.setColor(mClickColor);
            mPaint.setStrokeWidth(strokeWidthCircle);
            canvas.drawCircle(x, y, radius, mPaint);
            mPaint.setStrokeWidth(strokeWidthDefault);
        }

        //draw label
        mPaint.setFakeBoldText(false);
        mPaint.setColor(mTextLabelColor);
        paddingTop += (mDayHeight + mDayBaseHeight) / 2f;

        for (int i = 0; i < 7; i++) {
            x = (i + 0.5f) * mDayWidth + paddingLeft;
            y = paddingTop;
            int index = (i + mFirstDayOfWeek - 1) % 7;
            canvas.drawText(mLabels[index], x, y, mPaint);
//                canvas.drawText("AA", x, y, mPaint);
        }

        //draw date text
        int col = mFirstDayCol;
        int row = 1;
        int maxDay = mMaxAvailDay > 0 ? Math.min(mMaxAvailDay, mMaxDay) : mMaxDay;

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_VN_FORMAT);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH, mMonth);
        cal.set(Calendar.YEAR, mYear);
        LunarDate[] lunarDates = CalConvert.getMonthLunar(mMonth + 1, mYear);

        for (int day = 1; day <= mMaxDay; day++) {
            boolean isHoliday = CalConvert.isHoliday(day, mMonth + 1, mYear);
            if (isHoliday && mHolidayColor != 0) {
                mPaint.setColor(mHolidayColor);
            } else {
//                if (day == mSelectedDay || day == clickDay)
//                    mPaint.setColor(mTextHighlightColor);
                if (day < mMinAvailDay || day > maxDay)
                    mPaint.setColor(mTextDisableColor);
                else if (day == mToday)
                    mPaint.setColor(mSelectionColor);
                else
                    mPaint.setColor(mTextColor);
            }
            x = (col + 0.5f) * mDayWidth + paddingLeft;
            y = row * mDayHeight + paddingTop - 5;

            cal.set(Calendar.DAY_OF_MONTH, day);
            String date = sdf.format(cal.getTime());

            String textDay = getDayText(day);
            canvas.drawText(textDay, x, y, mPaint);

            mPaint.setTextSize(mTextSize * 0.6f);
            canvas.drawText(CalConvert.getLunarString(lunarDates[day - 1]), x + mTextSize * 0.5f, y + mTextSize * 0.5f, mPaint);

            float eventDotSize = mTextSize;

            if (mapEvent != null && mapEvent.containsKey(date)) {
                List<TKBEvent> eventList = mapEvent.get(date);

                if (eventList != null) {
                    Map<Integer, Integer> mapLine = new HashMap<>();
                    for (TKBEvent tkbEvent : eventList) {
                        int line = tkbEvent.getLineDraw();
                        if (mapLine.containsKey(tkbEvent.getLineDraw())) {
                            mapLine.put(line, mapLine.get(line) + 1);
                        } else {
                            mapLine.put(line, 1);
                        }
                    }
                    String circle = "•";
                    Rect bounds = new Rect();
                    mPaint.setTextSize(eventDotSize);
                    mPaint.getTextBounds(circle, 0, circle.length(), bounds);
                    for (Integer line : mapLine.keySet()) {
                        int numEvent = mapLine.get(line);
                        float withBegin = (bounds.width() * 1.3f) * (numEvent - 1) / 2f;
                        float yy = y - mTextSize * 0.7f - (bounds.height() * 1.3f) * line;
                        int count = 0;

                        for (TKBEvent event : eventList) {
                            if (event.getLineDraw() == line) {
                                mPaint.setColor(event.getColor());
                                float xx = x + (bounds.width() * 1.3f) * count - withBegin;
                                canvas.drawText(circle, xx, yy, mPaint);
                                count++;
                            }
                        }
                    }
                }
            } else {
//                System.out.println("map work null");
            }

            mPaint.setTextSize(mTextSize);

            col++;
            if (col == 7) {
                col = 0;
                row++;
            }
        }
    }

    private int getTouchedDay(float x, float y) {
//        float paddingTop = mDayPadding * 2 + mDayBaseHeight + getPaddingTop() + mDayHeight;
        float paddingTop = getPaddingTop() + mDayHeight;
        if (isMonthText) {
            paddingTop += mDayPadding * 2 + mDayBaseHeight;
        }
        if (x < getPaddingLeft() || x > getWidth() - getPaddingRight() || y < paddingTop || y > getHeight() - getPaddingBottom())
            return -1;

        int col = (int) Math.floor((x - getPaddingLeft()) / mDayWidth);
        int row = (int) Math.floor((y - paddingTop) / mDayHeight);
        int maxDay = mMaxAvailDay > 0 ? Math.min(mMaxAvailDay, mMaxDay) : mMaxDay;

        int day = row * 7 + col - mFirstDayCol + 1;
        if (day < 0 || day < mMinAvailDay || day > maxDay)
            return -1;

        return day;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mTouchedDay = getTouchedDay(event.getX(), event.getY());
                return true;
            case MotionEvent.ACTION_UP:
                if (getTouchedDay(event.getX(), event.getY()) == mTouchedDay && mTouchedDay > 0) {
                    if (onSelectDayListener != null) {
                        onSelectDayListener.selectDay(mTouchedDay, mMonth, mYear);
                        changeColorClickDay(mTouchedDay);
                    } else {
                        setSelectedDay(mTouchedDay, true);
                    }
                    mTouchedDay = -1;
                }
                return true;
            case MotionEvent.ACTION_CANCEL:
                mTouchedDay = -1;
                return true;
        }
        return true;
    }

    public void changeColorClickDay(int day) {
        clickDay = day;
        invalidate();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clickDay = -1;
                invalidate();
            }
        }, 15);
    }

    private void resetAnimation() {
        mStartTime = SystemClock.uptimeMillis();
        mAnimProgress = 0f;
    }

    private void startAnimation() {
        if (getHandler() != null) {
            resetAnimation();
            mRunning = true;
            getHandler().postAtTime(mUpdater, SystemClock.uptimeMillis() + ThemeUtil.FRAME_DURATION);
        }

        invalidate();
    }

    private void stopAnimation() {
        mRunning = false;
        mAnimProgress = 1f;
        if (getHandler() != null)
            getHandler().removeCallbacks(mUpdater);
        invalidate();
    }

    private final Runnable mUpdater = new Runnable() {

        @Override
        public void run() {
            update();
        }

    };

    private void update() {
        long curTime = SystemClock.uptimeMillis();
        mAnimProgress = Math.min(1f, (float) (curTime - mStartTime) / mAnimDuration);

        if (mAnimProgress == 1f)
            stopAnimation();

        if (mRunning) {
            if (getHandler() != null)
                getHandler().postAtTime(mUpdater, SystemClock.uptimeMillis() + ThemeUtil.FRAME_DURATION);
            else
                stopAnimation();
        }

        invalidate();
    }

    public String getDisplayName(Context context, int style, int field, int value) {
        String[] arr;
        if (field == CalConvert.DAY) {
            arr = CalConvert.getArrayName(context, style, CalConvert.DAY);
            if (value > 0) {
                return arr[value - 1];
            }
        } else {
            arr = CalConvert.getArrayName(context, style, CalConvert.MONTH);
            return arr[value];
        }

        return null;
    }

    private String getDayText(int day) {
        return day + "";
    }

    private void measureMonthView(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        measureBaseSize();

        int size = Math.round(Math.max(mDayBaseWidth, mDayBaseHeight));

        int width = size * 7 + mPaddingLeft + mPaddingRight;
        int height = Math.round(size * 7 + mDayBaseHeight + mDayPadding * 2 + mPaddingTop + mPaddingBottom);

        switch (widthMode) {
            case MeasureSpec.AT_MOST:
                width = Math.min(width, widthSize);
                break;
            case MeasureSpec.EXACTLY:
                width = widthSize;
                break;
        }

        switch (heightMode) {
            case MeasureSpec.AT_MOST:
                height = Math.min(height, heightSize);
                break;
            case MeasureSpec.EXACTLY:
                height = heightSize;
                break;
        }

        mMonthRealWidth = width;
        mMonthRealHeight = height;
    }

    private void measureBaseSize() {
        mPaint.setTextSize(mTextSize);
        mPaint.setTypeface(mTypeface);
        mDayBaseWidth = mPaint.measureText("88", 0, 2) + mDayPadding * 2;

        Rect bounds = new Rect();
        mPaint.getTextBounds("88", 0, 2, bounds);
//        mDayBaseHeight = bounds.height();
        mDayBaseHeight = 36;
    }

    public void setOnSelectDayListener(OnSelectDayListener onSelectDayListener) {
        this.onSelectDayListener = onSelectDayListener;
    }

    public interface OnSelectDayListener {
        void selectDay(int day, int month, int year);
    }

    private OnSelectDayListener onSelectDayListener;

}