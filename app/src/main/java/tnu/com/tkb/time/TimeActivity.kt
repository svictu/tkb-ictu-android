package tnu.com.tkb.time

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_time.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tnu.com.tkb.R
import tnu.com.tkb.api.TkbService
import tnu.com.tkb.util.PreferencesHelper

class TimeActivity : AppCompatActivity() {

    var isLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_time)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        swipeRefresh.setOnRefreshListener { getTime() }
        showTime()
        getTime()
    }

    private fun getTime() {
        beginLoad()
        isLoading = true

        var idSchool = ""
        val user = PreferencesHelper(this).getUser()
        if (user != null) {
            idSchool = user.id.substring(0, 3)
        }

        TkbService.create().getTimeAll(idSchool).enqueue(object : Callback<TimeAll> {
            override fun onResponse(call: Call<TimeAll>, response: Response<TimeAll>) {
                if (response.body() != null) {
                    saveTimeAll(Gson().toJson(response.body()))
                    finishLoad(getString(R.string.get_tkb_success), false)
                }
            }

            override fun onFailure(call: Call<TimeAll>, t: Throwable) {
                var msg = t.message
                if (msg != null && msg.contains("UnknownHostException")) {
                    msg = getString(R.string.no_internet)
                }
                if (msg == null) {
                    msg = getString(R.string.no_internet)
                }
                finishLoad(msg!!, true)
            }
        })
    }

    private fun saveTimeAll(timeAllJson: String) {
        PreferencesHelper(this).timeAllJson = timeAllJson
    }

    private fun beginLoad() {
        swipeRefresh.post { swipeRefresh.setRefreshing(true) }
    }

    private fun finishLoad(msg: String, isError: Boolean) {
        isLoading = false
        swipeRefresh.postDelayed({ swipeRefresh.isRefreshing = false }, 200)
        showTime()
    }

    private fun showTime() {
        val timeAll = PreferencesHelper(this).getTimeAll()
        if ((timeAll == null || timeAll.dayBeginSummer == 0) && !isLoading) {
            tvDateBegin.setText(R.string.can_not_load_time)
            return
        }
        if (timeAll != null) {
            tvDateBegin.text = getTextBeginTime(timeAll)
            rv.layoutManager = LinearLayoutManager(this)
            rv.adapter = TimeAdapter(this, timeAll)
        }
    }

    private fun getTextBeginTime(timeAll: TimeAll): String {
        var text = getString(R.string.day_begin_winner) + ": " + timeAll.dayBeginWinner + "/" + timeAll.monthBeginWinner
        text += "\n" + getString(R.string.day_begin_summer) + ": " + timeAll.dayBeginSummer + "/" + timeAll.monthBeginSummer
        return text
    }
}
