package tnu.com.tkb.time

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import tnu.com.tkb.R
import kotlinx.android.synthetic.main.item_time.view.*
import java.util.*

class TimeAdapter(val context: Context, val timeAll: TimeAll) : RecyclerView.Adapter<TimeAdapter.ItemHolder>() {

    override fun getItemCount(): Int {
        return timeAll.timeListSummer.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(LayoutInflater.from(context).inflate(R.layout.item_time, parent, false))
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        if (position == 0) {
            holder.tvTime.text = context.getText(R.string.time_item)
            holder.tvSummner.text = context.getText(R.string.summer)
            holder.tvWinner.text = context.getText(R.string.winner)
            holder.layoutItem.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
        } else {
            val timeSummer = timeAll.timeListSummer[position]
            val timeWinner = timeAll.timeListWinner[position]

            val textTimeSummner = getTextFromHourAndMinute(timeSummer.hour, timeSummer.minute) + " - " + getTimeEndLession(timeSummer)
            val textTimeWinner = getTextFromHourAndMinute(timeWinner.hour, timeWinner.minute) + " - " + getTimeEndLession(timeWinner)
            holder.tvTime.text = position.toString()
            holder.tvSummner.text = textTimeSummner
            holder.tvWinner.text = textTimeWinner
        }
        holder.layoutItem.setOnClickListener { }
    }

    private fun getTimeEndLession(timeItem: TimeItem): String {
        val calendar = Calendar.getInstance()
        calendar[Calendar.HOUR_OF_DAY] = timeItem.hour
        calendar[Calendar.MINUTE] = timeItem.minute
        calendar.add(Calendar.MINUTE, timeAll.minuteOfLession)
        val textTimeEnd = getTextFromHourAndMinute(calendar[Calendar.HOUR_OF_DAY], calendar[Calendar.MINUTE])
        return textTimeEnd
    }

    private fun getTextFromHourAndMinute(hour: Int, minute: Int): String {
        return hour.toString().padStart(2, '0') + ":" + minute.toString().padStart(2, '0')
    }

    class ItemHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvTime = view.tvTime
        val tvSummner = view.tvSummer
        val tvWinner = view.tvWinner
        val layoutItem = view.layoutItem
    }
}

