package tnu.com.tkb.time

import com.google.gson.annotations.SerializedName

data class TimeAll(
        @SerializedName("dayBeginSummer") val dayBeginSummer: Int,
        @SerializedName("monthBeginSummer") val monthBeginSummer: Int,
        @SerializedName("dayBeginWinner") val dayBeginWinner: Int,
        @SerializedName("monthBeginWinner") val monthBeginWinner: Int,
        @SerializedName("minuteOfLession") val minuteOfLession: Int,
        @SerializedName("summer") val timeListSummer: ArrayList<TimeItem>,
        @SerializedName("winner") val timeListWinner: ArrayList<TimeItem>
)