package tnu.com.tkb.time

import com.google.gson.annotations.SerializedName

data class TimeItem(
        @SerializedName("h") val hour: Int,
        @SerializedName("m") val minute: Int
)