package tnu.com.tkb.tkb

import android.app.AlertDialog
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tnu.com.tkb.R
import tnu.com.tkb.note.Note
import tnu.com.tkb.note.NoteActivity
import kotlinx.android.synthetic.main.item_tkb.view.*
import tnu.com.tkb.model.Tkb
import java.util.*

class TkbAdapter(val items: ArrayList<Any>, val context: Context) : RecyclerView.Adapter<TkbAdapter.ItemHolder>() {

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(LayoutInflater.from(context).inflate(R.layout.item_tkb, parent, false))
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        if (items[position] is Tkb) {
            val tkb = items[position] as Tkb

            holder.viewDot.setBackgroundResource(R.drawable.bg_event)
            holder.tvName.text = tkb.name
            holder.tvTime.text = context.getString(R.string.time_item) + ": " +  tkb.time
            holder.tvPlace.text = getPlaceText(tkb)
            holder.clickTkb(tkb)
        }
        if (items[position] is Note) {
            val note = items[position] as Note
            holder.tvName.text = note.content
            holder.viewDot.setBackgroundResource(R.drawable.bg_note)
            holder.clickNote(note)
        }

        holder.tvPlace.visibility = if (items[position] is Tkb) View.VISIBLE else View.GONE
        holder.tvTime.visibility = if (items[position] is Tkb) View.VISIBLE else View.GONE
    }

    private fun getPlaceText(tkb: Tkb): String {
        var placeText = tkb.place
        if (!TextUtils.isEmpty(tkb.teacher)) placeText += " - " + tkb.teacher
        if (!TextUtils.isEmpty(tkb.type)) placeText += " - " + tkb.type
        return placeText
    }

    class ItemHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val tvName = view.tvName
        val tvPlace = view.tvPlace
        val tvTime = view.tvTime
        val viewDot = view.viewDot

        fun clickTkb(tkb: Tkb) {
            view.setOnClickListener {
                AlertDialog.Builder(view.context)
                        .setTitle(R.string.event_info)
                        .setMessage(tkb.toInfo())
                        .setPositiveButton(R.string.ok, null)
                        .show()
            }
        }

        fun clickNote(note: Note) {
            view.setOnClickListener { NoteActivity.displayNoteActivity(view.context, note, note.date) }
        }
    }
}

