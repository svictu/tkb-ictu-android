package tnu.com.tkb.tkb

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import tnu.com.tkb.R
import tnu.com.tkb.calendarevent.MonthView
import tnu.com.tkb.calendarevent.TKBEvent
import tnu.com.tkb.util.DateConveter
import tnu.com.tkb.util.PreferencesHelper
import java.util.*

class MonthFragment : Fragment(), MonthView.OnSelectDayListener {
    private val calendar = Calendar.getInstance()
    private var monthView: MonthView? = null

    companion object {
        val KEY_MONTH = "month"
        val KEY_YEAR = "year"
        fun newInstance(month: Int, year: Int): MonthFragment {
            val fragment = MonthFragment()
            val args = Bundle()
            args.putInt(KEY_MONTH, month)
            args.putInt(KEY_YEAR, year)
            fragment.setArguments(args)
            return fragment
        }
    }

    private fun pareData() {
        val month = arguments!!.getInt(KEY_MONTH)
        val year = arguments!!.getInt(KEY_YEAR)
        calendar.set(year, month, 1)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        pareData()

        val lp = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        monthView = MonthView(context, calendar)
        monthView!!.setHolidayColor(ContextCompat.getColor(context as MainActivity, R.color.colorHoliday))
        monthView!!.layoutParams = lp
        monthView!!.setIsMonthText(true)
        monthView!!.setOnSelectDayListener(this)

        loadData()
        return monthView
    }

    fun loadData() {
        val colorEvent = ContextCompat.getColor(context!!, R.color.colorEvent)
        val colorNote = ContextCompat.getColor(context!!, R.color.colorNote)

        monthView!!.clearEvent()
        val user = PreferencesHelper(context!!).getUser()
        if (user != null) {
            for (tkb in user.tkb) {
                monthView!!.addEvent(tkb.date, TKBEvent().setColor(colorEvent))
            }
        }

        val noteList = PreferencesHelper(context!!).getNoteList()
        if (noteList != null) {
            for (note in noteList) {
                monthView!!.addEvent(note.date, TKBEvent().setColor(colorNote).setLineDraw(1))
            }
        }

        monthView!!.invalidate()
    }

    override fun selectDay(day: Int, month: Int, year: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, day)
        DayActivity.displayDayActivity(context!!, DateConveter().dateToText(calendar.time))
    }
}