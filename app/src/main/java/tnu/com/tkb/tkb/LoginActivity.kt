package tnu.com.tkb.tkb

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tnu.com.tkb.R
import tnu.com.tkb.api.TkbService
import tnu.com.tkb.model.User
import tnu.com.tkb.util.PreferencesHelper
import tnu.com.tkb.util.SmartIntent
import tnu.com.tkb.util.Url
import java.io.PrintWriter
import java.io.StringWriter

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        btnOk.setOnClickListener { getTkb() }
        tvVideoUse.setOnClickListener { SmartIntent().openUrl(this, Url.URL_VIDEO_HD) }
        dismissKeyboard(this, editId)
    }

    private fun getTkb() {
        val id = editId.text.toString().trim()
        if (TextUtils.isEmpty(id)) {
            Toast.makeText(this, R.string.pls_enter_id, Toast.LENGTH_SHORT).show()
        }

        beginLoad()

        TkbService.create().getUser(id).enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                try {
                    if (response.body() == null) {
                        finishLoad(getString(R.string.error_data), true)
                    } else {
                        val user = response.body()
                        for (tkb in user!!.tkb) {
                            tkb.date = tkb.date.replace("-", "/")
                        }
                        saveUserJson(Gson().toJson(user))
                        finishLoad(getString(R.string.get_tkb_success), false)
                    }
                } catch (e: Exception) {
                    val writer = StringWriter()
                    e.printStackTrace(PrintWriter(writer))
                    AlertDialog.Builder(this@LoginActivity)
                            .setMessage(writer.toString())
                            .show()
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                t.printStackTrace()
                val writer = StringWriter()
                t.printStackTrace(PrintWriter(writer))
                AlertDialog.Builder(this@LoginActivity)
                        .setMessage(writer.toString())
                        .show()

                var msg = "Fail: " + Gson().toJson(t)
                if (msg.contains("UnknownHostException")) {
                    msg = getString(R.string.no_internet)
                }
                if (msg == null) {
                    msg = getString(R.string.no_internet)
                }
                finishLoad(msg!!, true)
            }
        })
    }

    private fun saveUserJson(user: String) {
        PreferencesHelper(this).userJson = user
    }

    private fun beginLoad() {
        progressBar.visibility = View.VISIBLE
    }

    private fun finishLoad(msg: String, isError: Boolean) {
        progressBar.visibility = View.GONE
        if (!TextUtils.isEmpty(msg)) {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
        }
        if (!isError) {
            finishAndLoadTKB()
        }
    }

    private fun finishAndLoadTKB() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    fun dismissKeyboard(activity: Activity, editText: EditText) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(editText.windowToken, 0)
    }
}