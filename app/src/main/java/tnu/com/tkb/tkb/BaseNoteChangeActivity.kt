package tnu.com.tkb.tkb

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import tnu.com.tkb.note.Note
import tnu.com.tkb.note.NoteActivity

open class BaseNoteChangeActivity : AppCompatActivity() {
    companion object {
        val ACTION_EDIT_NOTE: String = "ACTION_EDIT_NOTE"
        val ACTION_DELETE_NOTE: String = "ACTION_DELETE_NOTE"
    }

    val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            val jsonNote = intent!!.getStringExtra(NoteActivity.KEY_NOTE)
            val note = Gson().fromJson<Note>(jsonNote, Note::class.java)

            Log.e("TKB note change note", jsonNote)
            Log.e("TKB note change action", intent.action)

            if (note != null) {
                when (intent.action) {
                    ACTION_EDIT_NOTE -> {
                        onEditNote(note)
                    }
                    ACTION_DELETE_NOTE -> {
                        onEditNote(note)
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        registerReceiver(broadCastReceiver, getIntentFilter())
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadCastReceiver)
    }

    private fun getIntentFilter(): IntentFilter {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_DELETE_NOTE)
        intentFilter.addAction(ACTION_EDIT_NOTE)
        return intentFilter
    }

    open fun onEditNote(note: Note) {}

    protected open fun onDeleteNote(note: Note) {}
}