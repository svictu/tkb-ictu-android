package tnu.com.tkb.tkb

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import tnu.com.tkb.R
import tnu.com.tkb.calendarevent.CalConvert
import tnu.com.tkb.note.Note
import tnu.com.tkb.note.NoteActivity
import tnu.com.tkb.util.DateConveter
import kotlinx.android.synthetic.main.activity_day.*
import java.util.*
import kotlin.collections.HashMap


class DayActivity : BaseNoteChangeActivity() {
    companion object {
        const val DAY_SELECT: String = "day"

        fun displayDayActivity(context: Context, day: String) {
            val intent = Intent(context, DayActivity::class.java)
            intent.putExtra(DAY_SELECT, day)
            context.startActivity(intent)
        }
    }

    private var day: String = ""
    private var selectDay: String = ""
    private var context: Context? = null

    private fun pareData() {
        day = intent.getStringExtra(DAY_SELECT)
        selectDay = day
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_day)
        setSupportActionBar(toolbar)
        context = this
        pareData()

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        val pageAdapter = SlidePageAdapter(this, supportFragmentManager, day)
        viewPager.adapter = pageAdapter
        viewPager.currentItem = SlidePageAdapter.NUM_PAGE / 2
        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                setTitleDay(getTitleByCalenar(pageAdapter.getCalendarByPos(position)!!))
                selectDay = DateConveter().dateToText(pageAdapter.getCalendarByPos(position)!!.time)
            }
        })
        val calendar = Calendar.getInstance()
        calendar.time = DateConveter().textToDate(day)
        setTitleDay(getTitleByCalenar(calendar))
        fab.setOnClickListener { NoteActivity.displayNoteActivity(this, null, selectDay) }
    }

    fun setTitleDay(title: String) {
        supportActionBar!!.title = ""
        tvToolbarTitle.text = title
    }

    fun getTitleByCalenar(cal: Calendar): String {
        val month = CalConvert.getArrayName(context, CalConvert.NUMBER, CalConvert.MONTH)[cal[Calendar.MONTH]]
        val title = month + " - " + cal[Calendar.YEAR]
        return title
    }

    override fun onEditNote(note: Note) {
        viewPager.adapter!!.notifyDataSetChanged()
    }

    override fun onDeleteNote(note: Note) {
        viewPager.adapter!!.notifyDataSetChanged()
    }

    class SlidePageAdapter(val context: Context, fm: FragmentManager, day: String) : FragmentStatePagerAdapter(fm) {
        companion object {
            val NUM_PAGE: Int = 100
        }

        val calendar = Calendar.getInstance()
        val mapCalendar = HashMap<Int, Calendar>()

        init {
            val date = DateConveter().textToDate(day)
            this.calendar.time = date
            calendar.add(Calendar.DATE, -NUM_PAGE / 2)
        }

        override fun getItem(position: Int): Fragment {
            val cal = Calendar.getInstance()
            cal.time = calendar.time
            cal.add(Calendar.DATE, position)
            mapCalendar.put(position, cal)
            return DayFragment.newInstance(DateConveter().dateToText(cal.time))
        }

        override fun getItemPosition(`object`: Any): Int {
            val f = `object` as DayFragment
            f.loadData()
            return super.getItemPosition(`object`)
        }

        override fun getCount(): Int {
            return NUM_PAGE
        }

        fun getCalendarByPos(position: Int): Calendar? {
            return mapCalendar[position]
        }
    }
}
