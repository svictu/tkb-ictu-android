package tnu.com.tkb.tkb

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import duc.cal.LunarDate
import tnu.com.tkb.R
import tnu.com.tkb.calendarevent.CalConvert
import tnu.com.tkb.util.DateConveter
import tnu.com.tkb.util.PreferencesHelper
import kotlinx.android.synthetic.main.content_day.view.*
import java.util.*

class DayFragment : Fragment() {
    private var day: String? = ""
    private var fragmentView: View? = null

    companion object {
        fun newInstance(day: String): DayFragment {
            val fragment = DayFragment()
            val args = Bundle()
            args.putString(DayActivity.DAY_SELECT, day)
            fragment.setArguments(args)
            return fragment
        }
    }

    private fun pareData() {
        day = arguments!!.getString(DayActivity.DAY_SELECT)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.content_day, container, false)
        fragmentView = view
        pareData()
        loadData()
        return view
    }

    @SuppressLint("SimpleDateFormat")
    fun loadData() {
        if (TextUtils.isEmpty(day)) {
            return
        }
        val calendar = Calendar.getInstance()
        calendar.time = DateConveter().textToDate(day!!)
        val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1
        val list = PreferencesHelper(context!!).getTkbByDay(day!!)

        fragmentView!!.tvDay.text = calendar.get(Calendar.DAY_OF_MONTH).toString()
        fragmentView!!.tvDayOfWeek.text = CalConvert.getArrayName(context, CalConvert.LONG, CalConvert.DAY)[dayOfWeek].toUpperCase()

        fragmentView!!.rv.layoutManager = LinearLayoutManager(context)
        fragmentView!!.rv.adapter = TkbAdapter(list, context!!)
        fragmentView!!.rv.isNestedScrollingEnabled = false

        if (!list.isEmpty()) {
            val numEventToday = context!!.getString(R.string.num_event_today).replace("%d", list.size.toString())
            fragmentView!!.tvNotify.text = numEventToday
            fragmentView!!.tvNotify.setTextColor(ContextCompat.getColor(context!!, R.color.colorEvent))
        } else {
            fragmentView!!.tvNotify.text = context!!.getString(R.string.no_event)
            fragmentView!!.tvNotify.setTextColor(ContextCompat.getColor(context!!, R.color.colorNote))
        }

        val lunarDate: LunarDate = CalConvert.getLunarDate(calendar[Calendar.DAY_OF_MONTH], calendar[Calendar.MONTH] + 1, calendar[Calendar.YEAR])
        val lunarText = lunarDate.day.toString() + "/" + lunarDate.month + " " + context!!.getString(R.string.lunar)
        fragmentView!!.tvLunarDay.text = lunarText


        val eventDay = CalConvert.getEvent(context, calendar.time)
        if (!TextUtils.isEmpty(eventDay)) {
            fragmentView!!.tvEventDay.text = eventDay
            fragmentView!!.tvEventDay.visibility = View.VISIBLE
            fragmentView!!.tvEventDay.setOnClickListener { Toast.makeText(context, eventDay, Toast.LENGTH_SHORT).show() }
        } else {
            fragmentView!!.tvEventDay.visibility = View.GONE
        }
    }
}