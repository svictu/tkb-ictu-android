package tnu.com.tkb.tkb

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.android.material.navigation.NavigationView
import tnu.com.tkb.BuildConfig
import tnu.com.tkb.R
import tnu.com.tkb.note.Note
import tnu.com.tkb.time.TimeActivity
import tnu.com.tkb.util.CheckUpdateApp
import tnu.com.tkb.util.PreferencesHelper
import tnu.com.tkb.util.SmartIntent
import tnu.com.tkb.util.Url
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import java.util.*

class MainActivity : BaseNoteChangeActivity(), NavigationView.OnNavigationItemSelectedListener {
    private val REQUEST_CODE_GET_TKB: Int = 1
    var context: Context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
        fab.setOnClickListener { gotoToday() }

        context = this

        setAdapterViewPage()
        tvToday.text = Calendar.getInstance()[Calendar.DAY_OF_MONTH].toString()

        val header = navView.getHeaderView(0)
        val appVersion = getString(R.string.app_name) + " - " + BuildConfig.VERSION_NAME
        header.tvUserName.text = appVersion

        CheckUpdateApp(this).checkUpdate()
    }

    private fun setAdapterViewPage() {
        viewPagerMonth.adapter = SlidePageAdapter(this, supportFragmentManager, Calendar.getInstance())
        viewPagerMonth.currentItem = SlidePageAdapter.NUM_PAGE / 2
        showInfoUser()
    }

    private fun showInfoUser() {
        val user = PreferencesHelper(this).getUser()
        if (user != null) {
            supportActionBar!!.title = user.name
            supportActionBar!!.subtitle = user.id
        } else {
            supportActionBar!!.title = getString(R.string.app_name)
            supportActionBar!!.subtitle = getString(R.string.app_info)
        }
    }

    private fun gotoToday() {
        setAdapterViewPage()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_get_tkb -> {
                startActivityForResult(Intent(this, LoginActivity::class.java), REQUEST_CODE_GET_TKB)
            }
            R.id.nav_time_in_out -> {
                startActivity(Intent(this, TimeActivity::class.java))
            }
            R.id.nav_del_tkb -> {
                AlertDialog.Builder(this)
                        .setTitle(R.string.delete_tkb)
                        .setMessage(R.string.sure_delete_tkb)
                        .setPositiveButton(R.string.ok) { dialog, which ->
                            run {
                                PreferencesHelper(context).userJson = ""
                                setAdapterViewPage()
                            }
                        }
                        .setNegativeButton(R.string.cancel, null)
                        .show()

            }
            R.id.nav_upload_tkb -> {
                AlertDialog.Builder(this)
                        .setTitle(R.string.upload)
                        .setMessage(R.string.select_upload_type)
                        .setPositiveButton(R.string.student) { dialog, which ->
                            run { SmartIntent().openUrl(context, Url.URL_UPLOAD_STUDENT) }
                        }
                        .setNegativeButton(R.string.teacher) { dialog, which ->
                            run { SmartIntent().openUrl(context, Url.URL_UPLOAD_TEACHER) }
                        }
                        .show()
            }
            R.id.nav_feedback -> {
                val uri = Uri.parse(Url.URL_FB_FEEDBACK)
                val intent = Intent(Intent.ACTION_VIEW, uri)
                try {
                    startActivity(intent)
                } catch (e: ActivityNotFoundException) {
                    Toast.makeText(context, R.string.pls_install_messager, Toast.LENGTH_SHORT).show()
                }
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == REQUEST_CODE_GET_TKB) {
            setAdapterViewPage()
        }
    }

    override fun onDeleteNote(note: Note) {
        viewPagerMonth.adapter!!.notifyDataSetChanged()
    }

    override fun onEditNote(note: Note) {
        viewPagerMonth.adapter!!.notifyDataSetChanged()
    }

    class SlidePageAdapter(val context: Context, fm: FragmentManager, private val calendar: Calendar) : FragmentStatePagerAdapter(fm) {
        companion object {
            const val NUM_PAGE: Int = 100
        }

        init {
            calendar.add(Calendar.MONTH, -NUM_PAGE / 2)
        }

        override fun getItem(position: Int): Fragment {
            val cal = Calendar.getInstance()
            cal.time = calendar.time
            cal.add(Calendar.MONTH, position)
            return MonthFragment.newInstance(cal[Calendar.MONTH], cal[Calendar.YEAR])
        }

        override fun getItemPosition(`object`: Any): Int {
            val f = `object` as MonthFragment
            f.loadData()
            return super.getItemPosition(`object`)
        }

        override fun getCount(): Int {
            return NUM_PAGE
        }
    }
}
