package tnu.com.tkb.api

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import tnu.com.tkb.model.User
import tnu.com.tkb.time.TimeAll
import tnu.com.tkb.util.CheckUpdateApp
import tnu.com.tkb.util.Url
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

interface TkbService {

    companion object Factory {
        fun create(): TkbService {
            val retrofit = Retrofit.Builder()
                    .client(getUnsafeOkHttpClient(60))
                    .addConverterFactory(GsonConverterFactory.create())
//                    .addConverterFactory(MoshiConverterFactory.create())
                    .baseUrl(Url.DOMAIN)
                    .build()

            return retrofit.create(TkbService::class.java)
        }

        fun getUnsafeOkHttpClient(timeout: Int): OkHttpClient {

            try {
                // Create a trust manager that does not validate certificate chains
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(
                            chain: Array<java.security.cert.X509Certificate>,
                            authType: String) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(
                            chain: Array<java.security.cert.X509Certificate>,
                            authType: String) {
                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate?> {
                        return arrayOfNulls(0)
                    }
                })

                // Install the all-trusting trust manager
                val sslContext = SSLContext.getInstance("TLS")
                sslContext.init(null, trustAllCerts, java.security.SecureRandom())
                // Create an ssl socket factory with our all-trusting manager
                val sslSocketFactory = sslContext.socketFactory

                var okHttpClient = OkHttpClient()
                okHttpClient = okHttpClient.newBuilder()
                        //                    .addInterceptor(new ConnectivityInterceptor(context))
                        .sslSocketFactory(sslSocketFactory)
                        .connectTimeout(timeout.toLong(), TimeUnit.SECONDS)
                        .writeTimeout(timeout.toLong(), TimeUnit.SECONDS)
                        .readTimeout(timeout.toLong(), TimeUnit.SECONDS)
                        .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
                        .build()

                return okHttpClient
            } catch (e: Exception) {
                throw RuntimeException(e)
            }

        }
    }


    @GET("/timetable.php")
    fun getUser(@Query("msv") msv: String): Call<User>

    @GET("/time.php")
    fun getTimeAll(@Query("school") schoolId: String): Call<TimeAll>

    @GET("/app.php?os=android")
    fun checkUpdate(): Call<CheckUpdateApp.UpdateApp>
}