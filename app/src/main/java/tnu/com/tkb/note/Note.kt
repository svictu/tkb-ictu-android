package tnu.com.tkb.note

import com.google.gson.annotations.SerializedName

data class Note(
        @SerializedName("id") var id: Long = 0,
        @SerializedName("content") var content: String = "",
        @SerializedName("date") var date: String = ""
)