package tnu.com.tkb.note

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_note.*
import tnu.com.tkb.R
import tnu.com.tkb.tkb.BaseNoteChangeActivity
import tnu.com.tkb.util.PreferencesHelper
import java.util.*
import kotlin.collections.ArrayList

class NoteActivity : AppCompatActivity() {

    companion object {
        const val KEY_NOTE: String = "note"
        const val KEY_DAY: String = "day"

        fun displayNoteActivity(context: Context, note: Note?, day: String) {
            val intent = Intent(context, NoteActivity::class.java)
            intent.putExtra(KEY_NOTE, Gson().toJson(note))
            intent.putExtra(KEY_DAY, day)
            context.startActivity(intent)
        }
    }

    var day: String = ""
    var note: Note? = null
    var isEdit: Boolean = true

    fun pareData() {
        day = intent.getStringExtra(KEY_DAY)

        val json = intent.getStringExtra(KEY_NOTE)
        if (!TextUtils.isEmpty(json)) {
            note = Gson().fromJson(json, Note::class.java)
        }
        if (note == null) {
            isEdit = false
            note = Note()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        pareData()
        editNote.setText(note!!.content)
        editNote.setSelection(note!!.content.length)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.note_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_save -> save()
            R.id.menu_delete -> showConfimDelete()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun save() {
        val content = editNote.text.toString().trim()
        if (TextUtils.isEmpty(content)) {
            Toast.makeText(this, R.string.note_empty, Toast.LENGTH_SHORT).show()
            return
        }

        val calendar = Calendar.getInstance()

        note!!.content = content
        if (!isEdit) {
            note!!.date = day
            note!!.id = calendar.timeInMillis
        }

        var noteList = PreferencesHelper(this).getNoteList()
        if (noteList == null) {
            noteList = ArrayList()
        }
        if (isEdit) {
            for (n in noteList) {
                if (n.id == note!!.id) {
                    noteList.remove(n)
                    break
                }
            }
        }
        noteList.add(note!!)
        PreferencesHelper(this).noteJson = Gson().toJson(noteList)

        sendActionChangeNote(BaseNoteChangeActivity.ACTION_EDIT_NOTE)
    }

    private fun showConfimDelete() {
        AlertDialog.Builder(this)
                .setTitle(R.string.delete)
                .setMessage(R.string.sure_delete)
                .setPositiveButton(R.string.delete) { dialog, which -> delete() }
                .setNegativeButton(R.string.cancel, null)
                .show()
    }

    private fun delete() {
        var noteList = PreferencesHelper(this).getNoteList()
        if (noteList == null) {
            noteList = ArrayList()
        }
        if (isEdit) {
            for (n in noteList) {
                if (n.id == note!!.id) {
                    noteList.remove(n)
                    break
                }
            }
        }
        PreferencesHelper(this).noteJson = Gson().toJson(noteList)
        sendActionChangeNote(BaseNoteChangeActivity.ACTION_DELETE_NOTE)
    }

    private fun sendActionChangeNote(action: String) {
        val intent = Intent(action)
        intent.putExtra(KEY_NOTE, Gson().toJson(note))
        sendBroadcast(intent)
        finish()
    }

    override fun onBackPressed() {
        save()
        super.onBackPressed()
    }
}
