package tnu.com.tkb.util

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.view.WindowManager
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import tnu.com.tkb.BuildConfig
import tnu.com.tkb.api.TkbService

class CheckUpdateApp(val context: Context) {

    fun checkUpdate() {

        TkbService.create().checkUpdate().enqueue(object : Callback<UpdateApp> {
            override fun onResponse(call: Call<UpdateApp>, response: Response<UpdateApp>) {
                if (response.body() != null) {
                    val updateApp = response.body()
                    if (updateApp != null && updateApp.versionCode > 0) {
                        saveCheckUpdate(Gson().toJson(updateApp))
                        Log.e("TKB update", "not need update ${updateApp.versionCode} / ${BuildConfig.VERSION_CODE}")
                    }
                    finishCheckUpdate()
                }
            }

            override fun onFailure(call: Call<UpdateApp>, t: Throwable) {
                t.printStackTrace()
                finishCheckUpdate()
            }
        })
    }

    private fun saveCheckUpdate(json: String) {
        PreferencesHelper(context).checkUpdateJson = json
    }

    private fun finishCheckUpdate() {
        val updateApp = PreferencesHelper(context).getUpdateApp()
        if (updateApp != null && updateApp.versionCode > BuildConfig.VERSION_CODE) {
            showAlertUpdate(updateApp)
        }
    }

    private fun showAlertUpdate(updateApp: UpdateApp) {
        val builder = AlertDialog.Builder(context)
                .setTitle(updateApp.title)
                .setMessage(updateApp.description)
                .setCancelable(!updateApp.isRequire())
                .setPositiveButton(updateApp.btnOk) { _, _ -> run { gotoPlaySptore(updateApp.url) } }

        if (!updateApp.isRequire()) {
            builder.setNegativeButton(updateApp.btnCancel, null)
        }
        try {
            builder.show()
        } catch (e: WindowManager.BadTokenException) {
            e.printStackTrace()
        }
    }

    private fun gotoPlaySptore(url: String) {
        Log.e("TKB update", url)
        SmartIntent().openUrl(context, url)
    }

    data class UpdateApp(
            @SerializedName("version_code") val versionCode: Int = 0,
            @SerializedName("version_name") val versionName: String,
            @SerializedName("require_with_version") val requireWithVersion: Int,
            @SerializedName("des") val description: String,
            @SerializedName("url") val url: String,
            @SerializedName("title") val title: String,
            @SerializedName("button_ok") val btnOk: String,
            @SerializedName("button_cancel") val btnCancel: String
    ) {
        fun isRequire(): Boolean {
            return BuildConfig.VERSION_CODE <= requireWithVersion
        }
    }
}

