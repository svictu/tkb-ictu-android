package tnu.com.tkb.util

import java.text.SimpleDateFormat
import java.util.*

class DateConveter {
    companion object {
        val DATE_VN: String = "dd/MM/yyyy"
        val DATE_TIME_VN: String = "dd/MM/yyyy h:m:i"
        val DATE_DATA: String = "dd-MM-yyyy"
    }

    public fun dateToText(date: Date): String {
        return dateToText(date, DATE_VN)
    }

    public fun dateToText(date: Date, template: String): String {
        return SimpleDateFormat(template).format(date)
    }

    public fun textToDate(text: String): Date {
        return textToDate(text, DATE_VN)
    }

    public fun textToDate(text: String, template: String): Date {
        return SimpleDateFormat(template).parse(text)
    }
}