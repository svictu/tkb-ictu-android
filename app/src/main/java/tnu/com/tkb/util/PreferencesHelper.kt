package tnu.com.tkb.util

import android.content.Context
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import tnu.com.tkb.BuildConfig
import tnu.com.tkb.note.Note
import tnu.com.tkb.time.TimeAll
import tnu.com.tkb.model.User

class PreferencesHelper(context: Context) {
    companion object {
        val DEVELOP_MODE = false
        private val USER_JSON = "${BuildConfig.APPLICATION_ID}.USER_JSON"
        private val NOTE_JSON = "${BuildConfig.APPLICATION_ID}.NOTE_JSON"
        private val TIME_ALL_JSON = "${BuildConfig.APPLICATION_ID}.TIME_ALL_JSON"
        private val CHECK_UPDATE_JSON = "${BuildConfig.APPLICATION_ID}.CHECK_UPDATE_JSON"
    }

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    var userJson = preferences.getString(USER_JSON, "")
        set(value) = preferences.edit().putString(USER_JSON, value).apply()

    var noteJson = preferences.getString(NOTE_JSON, "")
        set(value) = preferences.edit().putString(NOTE_JSON, value).apply()

    var timeAllJson = preferences.getString(TIME_ALL_JSON, "")
        set(value) = preferences.edit().putString(TIME_ALL_JSON, value).apply()

    var checkUpdateJson = preferences.getString(CHECK_UPDATE_JSON, "")
        set(value) = preferences.edit().putString(CHECK_UPDATE_JSON, value).apply()


    public fun getUpdateApp(): CheckUpdateApp.UpdateApp? {
        return Gson().fromJson<CheckUpdateApp.UpdateApp>(checkUpdateJson, CheckUpdateApp.UpdateApp::class.java)
    }

    public fun getTimeAll(): TimeAll? {
        return Gson().fromJson<TimeAll>(timeAllJson, TimeAll::class.java)
    }

    public fun getUser(): User? {
        return Gson().fromJson<User>(userJson, User::class.java)
    }

    public fun getNoteList(): ArrayList<Note>? {
        return Gson().fromJson<ArrayList<Note>>(noteJson, object : TypeToken<ArrayList<Note>>() {}.type)
    }

    public fun getTkbByDay(day: String): ArrayList<Any> {
        val list = ArrayList<Any>()

        val user = getUser()
        if (user != null) {
            for (tkb in user.tkb) {
                if (day.equals(tkb.date)) {
                    list.add(tkb)
                }
            }
        }

        val noteList = getNoteList()
        if (noteList != null) {
            for (note in noteList) {
                if (day.equals(note.date)) {
                    list.add(note)
                }
            }
        }
        return list
    }
}