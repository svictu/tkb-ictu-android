package tnu.com.tkb.util

class Url {
    companion object {
        val URL_FB_FEEDBACK = "fb-messenger://user/100025161402265"
        val URL_VIDEO_HD = "https://www.youtube.com/watch?v=Y8OHoqoO-c4"

        val DOMAIN = "https://tkb.cachhoc.net"
        val URL_UPLOAD_STUDENT = "$DOMAIN/upload.php"
        val URL_UPLOAD_TEACHER = "$DOMAIN/uploadteacher.php"
        val URL_GET_TKB = "$DOMAIN/timetable.php?msv="
//        val URL_GET_TIME = "$DOMAIN/time.php?school="
//        val URL_CHECK_UPDATE = "$DOMAIN/app.php?os=android"
    }
}