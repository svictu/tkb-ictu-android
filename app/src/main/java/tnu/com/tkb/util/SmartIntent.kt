package tnu.com.tkb.util

import android.content.Context
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent

class SmartIntent {
    fun openUrl(context: Context, url: String) {
        val builder = CustomTabsIntent.Builder()
        // set toolbar color and/or setting custom actions before invoking build()
        // Once ready, call CustomTabsIntent.Builder.build() to create a CustomTabsIntent
        val customTabsIntent = builder.build()
        // and launch the desired Url with CustomTabsIntent.launchUrl()
        customTabsIntent.launchUrl(context, Uri.parse(url))
    }
}